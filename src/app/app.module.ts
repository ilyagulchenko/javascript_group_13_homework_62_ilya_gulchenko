import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { HomeComponent } from './home/home.component';
import { GamesComponent } from './games/games.component';
import { NewGameComponent } from './games/new-game/new-game.component';
import { GameItemComponent } from './games/game-item/game-item.component';
import {FormsModule} from '@angular/forms';
import {GameService} from './shared/game.service';
import {RouterModule, Routes} from '@angular/router';
import { FooterComponent } from './footer/footer.component';
import { GameDetailsComponent } from './games/game-details/game-details.component';
import {EmptyGameComponent} from './games/empty-game.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'games', component: GamesComponent, children: [
      {path: '', component: EmptyGameComponent},
      {path: 'new', component: NewGameComponent},
      {path: ':id', component: GameDetailsComponent}
    ]},
];

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    HomeComponent,
    GamesComponent,
    NewGameComponent,
    GameItemComponent,
    FooterComponent,
    GameDetailsComponent,
    EmptyGameComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [GameService],
  bootstrap: [AppComponent]
})
export class AppModule { }
