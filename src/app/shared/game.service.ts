import {Game} from './game.model';
import {EventEmitter} from '@angular/core';

export class GameService {
  gamesChange = new EventEmitter<Game[]>();

  private games: Game[] = [
    new Game('Mafia II', 'https://upload.wikimedia.org/wikipedia/ru/thumb/d/d6/MafiaII.jpg/274px-MafiaII.jpg', 'NES', 'Mafia II — компьютерная игра в жанре приключенческого боевика с открытым миром, сочетающего в себе автомобильный симулятор и шутер от третьего лица.'),
    new Game('Battlefield™ 1', 'https://upload.wikimedia.org/wikipedia/ru/9/98/Battlefield_1.jpg', 'Sega Genesis', 'Battlefield 1 — мультиплатформенная компьютерная игра в жанре шутера от первого лица, пятнадцатая по счёту из серии игр Battlefield.'),
    new Game('Forza Horizon 5', 'https://cdn.kanobu.ru/c/d48405ca2da6a042d1e6224169fe4649/400x510/cdn.kanobu.ru/games/61000/804188c3-3ebc-413d-80ec-9bc3538a45e2.JPG', 'Super Nintendo', 'Forza Horizon 5 — компьютерная игра в жанре аркадного гоночного симулятора.')
  ]

  getGames() {
    return this.games.slice();
  }

  getGame(index: number) {
    return this.games[index];
  }

  addGame(game: Game) {
    this.games.push(game);
    this.gamesChange.emit(this.games);
  }
}
