import {Component} from '@angular/core';

@Component({
  selector: 'app-empty-game',
  template: `
    <h4>Games details</h4>
    <p>No platform is selected!</p>
  `
})

export class EmptyGameComponent {}
