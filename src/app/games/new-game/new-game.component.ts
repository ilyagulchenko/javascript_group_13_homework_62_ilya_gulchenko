import {Component, ElementRef, EventEmitter, Output, ViewChild} from '@angular/core';
import {Game} from '../../shared/game.model';
import {GameService} from '../../shared/game.service';

@Component({
  selector: 'app-new-game',
  templateUrl: './new-game.component.html',
  styleUrls: ['./new-game.component.css']
})
export class NewGameComponent {
  @ViewChild('nameInput') nameInput!: ElementRef;
  @ViewChild('imageUrlInput') imageUrlInput!: ElementRef;
  @ViewChild('platformInput') platformInput!: ElementRef;
  @ViewChild('descriptionInput') descriptionInput!: ElementRef;

  constructor(public gameService: GameService) {}

  createGame() {
    const name = this.nameInput.nativeElement.value;
    const image = this.imageUrlInput.nativeElement.value;
    const platform = this.platformInput.nativeElement.value;
    const description = this.descriptionInput.nativeElement.value;

    const game = new Game(name, image, platform, description);
    this.gameService.addGame(game);
  }

}
